# Projeto Kubernetes dio

Projeto tem a finalidade de desenvolver um Pipeline de Deploy com GitLab e Kubernetes

## Requisitos:
- Cluster Kubernetes na GCP
- VM na GCP
- GitLab
- VSCode
- Putty

## Configurações iniciais 

### Pré configurações obrigatórias na VM

será nescessário instalar as seguintes aplicações na VM:
| Sistema | Comando |
| ------ | ------ |
| Git | ```sudo apt-get install git -y``` |
| kubectl | ```sudo apt-get install -y kubectl``` |
| Gcloud sdk | ```sudo apt-get install google-cloud-sdk-gke-gcloud-auth-plugin``` |

Sera nescessário realizar o login no gcloud

```sh
gcloud auth login
```

E por fim conectar ao cluster criado na GCP

```sh
gcloud container clusters get-credentials cluster-1 --zone southamerica-east1-a --project white-position-******
```

### Iniciando deploy

o arquivo de configuração **.gitlab-ci.yml** devera ser configurado com dados de acesso ao docker
e as chaves previamentes adicionadas nas variaveis de configuração CI/CD do gitlab

o deploy se iniciará automaticamente após realização do **push** ao gitlab

### Execução do projeto

o projeto foi configurado em nodePort, para rodar o projeto execute na porta 30005
> ip de exemplo, altere o ip conforme seu ip externo

```sh
34.151.247.250:30005
```

para liberar o firewall na porta 30005 execute o seguinte comando:
```sh
 gcloud compute firewall-rules create app --allow tcp:30005
```

## Imagem

![Exemplo aplicação](https://gitlab.com/luminato1/projeto2-dio/-/raw/main/images/exemplo.png)

## Autor

Projeto desenvolvido para atividade da Dio, criado por [Fabricio Luminato]

[Fabricio Luminato]: <www.linkedin.com/in/fabricioluminato>